<?php
	require_once "config.php";
	
	if(!isset($_SESSION["user_emailid"]))
	{
		header("location: index.php");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            $logout_date   = date('Y/m/d H:i:s');
            $empid=$_SESSION["user_emailid"];
            
            $query="UPDATE tbl_users set logout_date='$logout_date', logout_status='0' where user_emailid='$empid'";
            $res = mysqli_query($link, $query) or die(mysqli_error($link));

            unset($_SESSION["user_name"]);
            unset($_SESSION["user_empid"]);
            unset($_SESSION["user_loc"]);
            
            header("location: index.php");
            exit;
        }

    }
	
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="icon" href="img/favicon.png" type="image/png">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Product Videos</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-light">
  <a class="navbar-brand" href="#"><img src="img/logo.png" class="logo"></a>
  <div class="" id="navbarSupportedContent">
  </div>
</nav>
<div class="container-fluid">
    <div class="row mt-1 mb-1">
        <div class="col-12 text-right">
            Hello <?php echo $_SESSION['user_name']; ?>! <a href="?action=logout" class="btn btn-sm btn-danger">Logout</a> <a href="contact.php" class="btn btn-sm btn-primary">Contact Us</a>
        </div>
    </div>
    <div class="row video-panel">
        <div class="col-12 col-lg-4 text-center">
            <div class="embed-responsive embed-responsive-16by9">
            </div>
            <h3>Coact Virtual Webinar Solution</h3>    
        </div>
        <div class="col-12 col-lg-4 text-center">
            <div class="embed-responsive embed-responsive-16by9">
              <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/429868485" autoplay="false" allowfullscreen scrolling="no"></iframe>
            </div>    
            <h3>Virtual Background with Multi Layout</h3>    
        </div>
        
        <div class="col-12 col-lg-4 text-center">
            <div class="embed-responsive embed-responsive-16by9">
              <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/429868517" autoplay="false" allowfullscreen scrolling="no"></iframe>
            </div>    
            <h3>Coact Virtual Event</h3>    
        </div>
        <div class="col-12 col-lg-4 text-center">
            <div class="embed-responsive embed-responsive-16by9">
              <iframe class="embed-responsive-item" src="videos/Capgemini Lobby Camera 02.mp4" autoplay="false" allowfullscreen scrolling="no"></iframe>
            </div>    
            <h3>Coact Virtual Event</h3>    
        </div>
        <div class="col-12 col-lg-4 text-center">
            <div class="embed-responsive embed-responsive-16by9">
              <iframe class="embed-responsive-item" src="videos/Capgemini Lobby Camera 01.mp4" autoplay="false" allowfullscreen scrolling="no"></iframe>
            </div>    
            <h3>Coact Virtual Event</h3>    
        </div>
        <div class="col-12 col-lg-4 text-center">
            <div class="embed-responsive embed-responsive-16by9">
              <iframe class="embed-responsive-item" src="videos/Integrace City Tour ( Revised Integrace Logo on HO ).mp4"  autoplay="false" allowfullscreen scrolling="no"></iframe>
            </div>    
            <h3>Coact Virtual Event</h3>    
        </div>
        <div class="col-12 col-lg-4 text-center">
            <div class="embed-responsive embed-responsive-16by9 "autoplay="false">
              <iframe class="embed-responsive-item" src="videos/Lobby Walkthrough.mp4" autoplay="false" allowfullscreen scrolling="no"></iframe>
            </div>    
            <h3>Coact Virtual Event</h3>    
        </div>
		<div class="col-12 col-lg-4 text-center">
            <div class="embed-responsive embed-responsive-16by9">
            
            </div>    
            <a href="links.php"><h3>Coact Event links</h3></a>   
        </div>
        
        
    </div>
    
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(function(){

	$(document).on('submit', '#question-form', function()
    {  
            $.post('submitques.php', $(this).serialize(), function(data)
            {
                if(data=="success")
                {
                  $('#message').removeClass('alert-danger fail');
                  $('#message').addClass('alert-success success'); 
                  $('#message').text('Your question is submitted successfully.').fadeIn().delay(2000).fadeOut();
                  $('#question-form').find("textarea").val('');
                }
                else 
                {
                  $('#message').addClass('alert-danger fail');
                  $('#message').removeClass('alert-success success'); 
                  $('#message').text(data);
                }
                
            });
        
      
      return false;
    });
});
function update()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'update'},
         type: 'post',
         success: function(output) {
			   /*if(output=="0")
			   {
				   location.href='index.php';
			   }*/
         }
});
}
setInterval(function(){ update(); }, 30000);

</script>
</body>
</html>