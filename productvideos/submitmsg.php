<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require 'vendor/autoload.php';

require_once "config.php";

$name = $_POST['user_name'];
$name = mysqli_real_escape_string($link,$name);

$emailid = $_POST['user_emailid'];
$emailid = mysqli_real_escape_string($link,$emailid);

$msg = trim($_POST['contactmsg']);
$msg = mysqli_real_escape_string($link,$msg);

$sub = trim($_POST['subject']);
$sub = mysqli_real_escape_string($link,$sub);


$today=date("Y/m/d H:i:s");

$query="insert into tbl_questions(user_name, user_emailid, subject,  user_question, asked_at, eventname) values ('$name','$emailid','$sub','$msg','".$today."','$event_name') ";
$res = mysqli_query($link, $query) or die(mysqli_error($link));
$ques_id = mysqli_insert_id($link);
//echo $query;
if($ques_id > 0) 
{
    
  //send mail  
  $mail = new PHPMailer(true);
  
  $email = 'pawan@coact.co.in';
        
  try {
      //Server settings
      //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
      $mail->isSMTP();                                            // Send using SMTP
      $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
      $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
      $mail->Username   = 'support@coact.co.in';                     // SMTP username
      $mail->Password   = 'coact2020';                               // SMTP password
      $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS; //STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
      $mail->Port       = 465;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
  
      //Recipients
      $mail->setFrom('support@coact.co.in', 'Coact Product videos');
      $mail->addAddress($email);     // Add a recipient
  
      // Content
      $mail->isHTML(true);                                  // Set email format to HTML
      $mail->Subject = "New message on Product Videos portal";
      
      $message = "You have received a new message on Product Videos portal.";
      $message .="<br><br>";
      $message .="Name: " . $name . "<br>";
      $message .="Email ID: " . $emailid . "<br>";
      $message .="Subject: " . $sub . "<br>";
      $message .="Message: " . $msg . "<br>";
      
      $mail->Body    = $message;
  
      if($mail->send()){
        echo "success";	
      }
      else{
          echo "Mail could not be sent. But message has been saved.";
      }

  } 
  catch (Exception $e) 
  {
      echo "Mail could not be sent. But message has been saved.";
  }
    
}


 ?>
