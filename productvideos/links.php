<?php
	require_once "config.php";
	
	if(!isset($_SESSION["user_emailid"]))
	{
		header("location: index.php");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            $logout_date   = date('Y/m/d H:i:s');
            $empid=$_SESSION["user_emailid"];
            
            $query="UPDATE tbl_users set logout_date='$logout_date', logout_status='0' where user_emailid='$empid'";
            $res = mysqli_query($link, $query) or die(mysqli_error($link));

            unset($_SESSION["user_name"]);
            unset($_SESSION["user_empid"]);
            unset($_SESSION["user_loc"]);
            
            header("location: index.php");
            exit;
        }

    }
	
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="icon" href="img/favicon.png" type="image/png">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Product Links</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<style>
a {
    color: #000;
	font-size: 20px;
    text-decoration: none;
    background-color: transparent;
    -webkit-text-decoration-skip: objects;
}
td, th {
  
  text-align: left;
  padding: 2px;
}
</style>
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-light">
  <a class="navbar-brand" href="#"><img src="img/logo.png" class="logo"></a>
  <div class="" id="navbarSupportedContent">
  </div>
</nav>
<div class="container-fluid">
    <div class="row mt-1 mb-1">
        <div class="col-12 text-right">
            Hello <?php echo $_SESSION['user_name']; ?>! <a href="?action=logout" class="btn btn-sm btn-danger">Logout</a> <a href="contact.php" class="btn btn-sm btn-primary">Contact Us</a>
        </div>
    </div>
	
    <div class="row video-panel offset-md-6">
        <div class="col-md-4">
        <table >
		 <tr><td><a href="https://coact.live/GynacDEMO/">https://coact.live/GynacDEMO/</td></tr>
		 <tr><td><a href="https://coact.live/pharma/">https://coact.live/pharma/</td></tr>
		 <tr><td><a href="https://coact.live/DEMOCBM/">https://coact.live/DEMOCBM/</td></tr>
		 <tr><td><a href="https://titanannualawards2020.com/test/login/index.php">https://titanannualawards2020.com/test/login/index.php</td></tr>
		 <tr><td><a href="https://aims2020.onconf.in/ymvirtual/">https://aims2020.onconf.in/ymvirtual/</td></tr>
		 <tr><td><a href="https://coact.live/philips/sieindiasubcontinent/">https://coact.live/philips/sieindiasubcontinent/</td></tr>
		
		 <tr><td><a href="https://coact.live/LOREAL/">https://coact.live/LOREAL/</td></tr>
		 <tr><td><a href="https://coact.live/Michaelkors/">https://coact.live/Michaelkors/</td></tr>
		 <tr><td><a href="https://coact.live/dis-AIRCLaunch/register.php">https://coact.live/dis-AIRCLaunch/register.php</td></tr>
		 <tr><td><a href="https://coact.live/dis-%20mammographyusermeet/register.php">https://coact.live/mammographyusermeet/register.php</td></tr>
		 <tr><td><a href="https://coact.live/siemens/index.php?first_name=Viraj&last_name=K&email=viraj@coact.co.in">https://coact.live/siemens/</td></tr>
		
		 <tr><td><a href="https://coact.live/thyssenkrupp.elevator/">https://coact.live/thyssenkrupp.elevator/</td></tr>
		 <tr><td><a href="https://coact.live/millennia/">https://coact.live/millennia/</td></tr>
		 <tr><td><a href="https://coact.live/ncdex/register.php">https://coact.live/ncdex/register.php</td></tr>
		 <tr><td><a href="https://coact.live/Skoda/Volkswagen/">https://coact.live/Skoda/Volkswagen/</td></tr>
		 <tr><td><a href="https://coact.live/vitalmedcom/">https://coact.live/vitalmedcom/</td></tr>
		 <tr><td><a href="https://coact.live/ffdgroupsessions/">https://coact.live/ffdgroupsessions/</td></tr>
		 <tr><td><a href="https://educationportal.freedomfromdiabetes.org/">https://educationportal.freedomfromdiabetes.org/</td></tr>
         <tr><td><a href="https://coact.live/coactgames/login.php">https://coact.live/coactgames/login.php</td></tr>
		</table>
		</div>
    </div>
    
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(function(){

	$(document).on('submit', '#question-form', function()
    {  
            $.post('submitques.php', $(this).serialize(), function(data)
            {
                if(data=="success")
                {
                  $('#message').removeClass('alert-danger fail');
                  $('#message').addClass('alert-success success'); 
                  $('#message').text('Your question is submitted successfully.').fadeIn().delay(2000).fadeOut();
                  $('#question-form').find("textarea").val('');
                }
                else 
                {
                  $('#message').addClass('alert-danger fail');
                  $('#message').removeClass('alert-success success'); 
                  $('#message').text(data);
                }
                
            });
        
      
      return false;
    });
});
function update()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'update'},
         type: 'post',
         success: function(output) {
			   /*if(output=="0")
			   {
				   location.href='index.php';
			   }*/
         }
});
}
setInterval(function(){ update(); }, 30000);

</script>
</body>
</html>