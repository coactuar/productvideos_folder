-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 21, 2022 at 02:38 PM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `productvideos`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_emailid` varchar(255) NOT NULL,
  `subject` varchar(200) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `user_name`, `user_emailid`, `subject`, `user_question`, `asked_at`, `eventname`, `speaker`, `answered`) VALUES
(43, 'Sohail Samie Sayed', 'sayedsohail7@gmail.com', 'Inquiry', 'Need to discuss a brief', '2020-06-25 21:16:33', 'prodvids', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_phone` varchar(15) NOT NULL,
  `user_emailid` varchar(500) NOT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_phone`, `user_emailid`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(22, 'priyanka', '446357456374', 'pooja@coact.co.in', '2020-06-17 15:00:13', '2022-01-10 14:17:25', '2022-01-10 14:17:55', 1, 'prodvids'),
(23, 'PAWAN SHILWANT', '09545329222', 'pawan@coact.co.in', '2020-06-17 15:06:54', '2021-06-07 14:18:48', '2021-06-07 14:19:18', 1, 'prodvids'),
(24, 'viraj', '9765875731', 'viraj@coact.co.in', '2020-06-17 15:32:50', '2020-11-30 13:10:48', '2020-11-30 13:11:18', 1, 'prodvids'),
(25, 'Sujatha', '9845563760', 'sujatha@coact.co.in', '2020-06-17 15:49:17', '2021-12-16 11:44:54', '2021-12-16 11:45:24', 1, 'prodvids'),
(26, 'Pawan ', '9545329222', 'pawan@gmail.com', '2020-06-17 20:44:46', '2020-06-17 20:44:46', '2020-06-17 20:45:16', 1, 'prodvids'),
(27, 'Pravin Mukundrao Gangarde', '8983308877', 'pmgangarde@gmail.com', '2020-06-17 20:46:57', '2020-06-17 20:47:27', '2020-06-17 20:47:57', 1, 'prodvids'),
(28, 'Megha Dalvi', '9833987130', 'megha.87@gmail.com', '2020-06-17 20:55:09', '2020-06-17 20:55:09', '2020-06-17 20:55:39', 1, 'prodvids'),
(29, 'Abhijeet Choure', '9552598350', 'abhijeetchoure@gmail.com', '2020-06-17 21:02:56', '2020-06-17 21:02:56', '2020-06-17 21:03:26', 1, 'prodvids'),
(30, 'Nilambari', '8050507014', 'nilambari.mane2011@gmail.com', '2020-06-17 21:21:52', '2020-06-17 21:21:52', '2020-06-17 21:22:22', 1, 'prodvids'),
(31, 'Mahesh Bhuti', '+12484213846', 'maheshbhuti@gmail.com', '2020-06-18 13:13:43', '2020-06-18 13:13:43', '2020-06-18 13:14:13', 1, 'prodvids'),
(32, 'Pawan ', '9545329222', 'p@gmail.com', '2020-06-18 18:51:54', '2020-06-21 11:36:28', '2020-06-21 11:36:58', 1, 'prodvids'),
(37, 'Akshay Goel', '07838852770', 'akshay.goel@sonalika.com', '2020-07-07 21:03:24', '2020-07-11 11:39:57', '2020-07-11 11:40:27', 1, 'prodvids'),
(33, 'hariharan', '9010468956', 'hariiyengar39@gmail.com', '2020-06-25 21:05:40', '2020-06-25 21:05:40', '2020-06-25 21:06:10', 1, 'prodvids'),
(34, 'Sohail Samie Sayed', '09833701784', 'sayedsohail7@gmail.com', '2020-06-25 21:14:41', '2020-06-25 21:14:41', '2020-06-25 21:15:11', 1, 'prodvids'),
(35, 'Akshat Jharia', '7204420017', 'akshatjharia@gmail.com', '2020-06-27 15:37:01', '2021-09-28 20:09:13', '2021-09-28 20:09:43', 1, 'prodvids'),
(36, 'Bhushan ', '9137012033', 'bhushan.l@realene.com', '2020-06-30 17:01:18', '2020-06-30 17:01:18', '2020-06-30 17:01:48', 1, 'prodvids'),
(38, 'sarok', '7717307957', 'saroj.mohanty@sonalika.com', '2020-07-11 11:42:07', '2020-07-11 11:42:07', '2020-07-11 11:42:37', 1, 'prodvids'),
(39, 'Reynold', '09611518064', 'reynold@coact.co.in', '2020-07-14 14:23:41', '2021-05-10 11:40:03', '2021-05-10 11:40:33', 1, 'prodvids'),
(40, 'DINESH PATADIA', '09892495724', 'dineshp@inceptionx.in', '2020-07-16 12:51:31', '2020-07-16 12:51:31', '2020-07-16 12:52:01', 1, 'prodvids'),
(41, 'Sharad Tibdewal', '9819824801', 'sharad.tibdewal@hopintown.com', '2020-07-20 13:07:03', '2020-07-20 13:07:03', '2020-07-20 13:07:33', 1, 'prodvids'),
(42, 'Rina Shahi', '7661902233', 'rinashahi.ash@gmail.com', '2020-07-21 11:59:12', '2020-07-21 11:59:12', '2020-07-21 11:59:42', 1, 'prodvids'),
(43, 'Manmeet Singh', '9665049200', 'manmeets@britindia.com', '2020-08-12 15:58:53', '2020-08-12 15:58:53', '2020-08-12 15:59:23', 1, 'prodvids'),
(44, 'Yash Kapoor', '09999832887', 'yash.k@epicindiaevents.com', '2020-08-26 15:13:20', '2020-08-26 15:13:20', '2020-08-26 15:13:50', 1, 'prodvids'),
(45, 'Manoj Pathni', '9844087424', 'manojpathni@gmail.com', '2020-08-28 17:40:41', '2020-09-05 16:13:08', '2020-09-05 16:13:38', 1, 'prodvids'),
(46, 'Anand', '9243701760', 'ankr.desai@gmail.com', '2020-09-04 12:20:25', '2020-09-04 12:20:25', '2020-09-04 12:20:55', 1, 'prodvids'),
(47, 'Thilak Mathivanan', '9845873024', 'thilakm@gmail.com', '2020-09-07 14:10:39', '2020-09-07 14:31:39', '2020-09-07 14:32:09', 1, 'prodvids'),
(48, 'Saurabh', '9403151093', 'saurabhdpuranik@gmail.com', '2020-09-12 20:42:09', '2021-04-29 21:52:39', '2021-04-29 21:53:09', 1, 'prodvids'),
(49, 'Mujeeb', '9845190417', 'mujeeb.razack@gmail.com', '2020-10-02 16:33:40', '2020-10-02 16:33:40', '2020-10-02 16:34:10', 1, 'prodvids'),
(50, 'avinash', '9686956836', 'avinash@stagestudio.in', '2020-10-16 15:00:17', '2020-10-16 15:00:17', '2020-10-16 15:00:47', 1, 'prodvids'),
(51, 'siddu', '9060999404', 'mbsiddaramaiah@pes.edu', '2020-11-03 08:52:51', '2020-11-03 08:52:51', '2020-11-03 08:53:21', 1, 'prodvids'),
(52, 'Ravi', '9930955685', 'ravi.girdhar@capgemini.com', '2020-11-25 17:23:45', '2020-11-25 17:23:45', '2020-11-25 17:24:15', 1, 'prodvids'),
(53, 'Nishanth', '9742398865', 'nishanth.1911@gmail.com', '2020-12-24 11:14:25', '2020-12-24 11:14:25', '2020-12-24 11:14:55', 1, 'prodvids'),
(54, 'V', '6546', 'test1@coact.co.in', '2021-03-04 15:22:57', '2021-04-03 14:53:46', '2021-04-03 14:54:16', 1, 'prodvids'),
(55, 'Tanushree', '7795702662', 'tanushree@coact.co.in', '2021-03-16 15:15:27', '2021-04-26 13:45:20', '2021-04-26 13:45:50', 1, 'prodvids'),
(56, 'Nishanth', '8747973536', 'nishanth@coact.co.in', '2021-03-16 18:40:36', '2021-03-16 18:40:36', '2021-03-16 18:40:42', 0, 'prodvids'),
(57, 'neeraj', '08367773537', 'pandurevanthreddy002@gmail.com', '2021-05-12 11:49:12', '2021-08-17 15:17:49', '2021-08-17 15:18:19', 1, 'prodvids'),
(58, 'neeraj', '08367773537', 'neeraj@gmail.com', '2021-07-23 16:26:01', '2021-07-23 16:26:01', '2021-07-23 16:26:31', 1, 'prodvids'),
(59, 'neeraj', '09700467764', 'naganeerajreddy9989@gmail.com', '2021-08-26 11:34:37', '2021-10-11 17:30:03', '2021-10-11 17:30:33', 1, 'prodvids'),
(60, 'Jagadish', '9743598479', 'jagadish@coact.co.in', '2021-09-04 10:36:59', '2021-09-28 12:02:58', '2021-09-28 12:03:28', 1, 'prodvids'),
(61, 'Likitha', '8105278762', 'likitha@coact.co.in', '2021-11-30 09:58:19', '2022-01-05 14:46:34', '2022-01-05 14:47:04', 1, 'prodvids'),
(62, 'Shankar Iyer', '09769905416', 'shankar.iyer@icertis.com', '2021-12-07 17:59:39', '2021-12-07 17:59:39', '2021-12-07 18:00:09', 1, 'prodvids'),
(63, 'Neeraj', '8247524234', 'neeraj@coact.co.in', '2021-12-22 12:24:16', '2021-12-28 15:04:31', '2021-12-28 15:05:01', 0, 'prodvids'),
(64, 'neeraj', '888888888', 'reddy@gmail.com', '2021-12-27 16:23:41', '2021-12-27 16:23:41', '2021-12-27 16:24:11', 1, 'prodvids'),
(65, 'pooja', '9ueb834\\', 'pooja@coact.coin', '2022-01-06 10:58:33', '2022-01-06 10:58:33', '2022-01-06 10:59:03', 1, 'prodvids');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
